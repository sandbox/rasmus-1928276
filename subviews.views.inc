<?php
/**
 * @file
 * Defines the View Style Plugins for the Subviews module.
 */

/**
 * Implements hook_views_plugins().
 */
function subviews_views_plugins() {

  return array(
    'style' => array(
      'subviews' => array(
        'title' => t('Subviews'),
        'help' => t('Display the results as a table with a subview.'),
        'handler' => 'subviews_plugin_style_subviews',
        'uses row plugin' => FALSE,
        'uses row class' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'table',
        'path' => drupal_get_path('module', 'subviews'),
        'theme' => 'subviews',
        'theme path' => drupal_get_path('module', 'subviews') . '/theme',
        'theme file' => 'subviews.theme.inc',
      ),
    ),
  );
}
