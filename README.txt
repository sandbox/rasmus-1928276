The Subviews Module delivers an output style that lets you create table like
trees where views can be nested within eachother.

The output style contains the following features:
- expand/collapse functionality
- optional AJAX loading
- parsing field values as arguments to the subview
- remembering the expand/collapse state of a row in the user session 
- mixed entity views. The nested views doesn't have to be the same entity
  type as the parent view.
- no javascript fallback behavior

Example ussages
- Taxonomy trees
- Taxonomy lists that display related content 
- Node views with expandable comment lists
- Use it with Entity Reference to show related content


You need Views 3 for this module to work.

Use this module at your own risk.

Instructions
- Set up a contextual filter on the childview
- On your parent view select the output style Subviews
- If you haven't already done so, add the field to the parent view
  that you want to be passed to the child views contextual filter
- In the output style settings of the parent view select the
  child view and select the field(s) that you want to use as the
  argument(s) for the child view

Author
Rasmus Steen Kristensen (Co-founder at amsel dot dk)
http://drupal.org/user/2496238
