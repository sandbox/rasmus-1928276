<?php

/**
 * @file
 * Contains the Subviews style plugin.
 */

/**
 * Style plugin to render each item in a subview.
 *
 * @ingroup views_style_plugins
 */
class subviews_plugin_style_subviews extends views_plugin_style_table {

  // Set default options.
  function option_definition() {
    $options = parent::option_definition();

    // Load all include files from views subviews addons.
    module_load_all_includes('subviews.inc');

    // Call every module using hook_subviews_option_definition and merge.
    // It's return value with the other default options.
    return array_merge($options, module_invoke_all('subviews_option_definition'));
  }

  // Build the settings form for the view.
  function options_form(&$form, &$form_state) {
    // Include ctools dependent support.
    ctools_include('dependent');

    // Load up all views subviews modules functions.
    module_load_all_includes('subviews.inc');
    parent::options_form($form, $form_state);

    // Wrap all the form elements to help style the form.
    $form['subviews_wrapper'] = array(
      '#markup' => '<div id="views-subviews-form-wrapper">',
    );

    // View to be used as subview.
    $form['subviews_subview_header'] = array(
      '#markup' => '<h2>' . t('Subview') . '</h2>',
    );

    // Get a list of all available views.
    $views_info = $this->subviews_get_views();
    foreach ($views_info as $view => $name) {
      $views[$view] = $name;
    }
    asort($views);

    // Create the drop down box so users can choose an available view.
    $form['subviews_view'] = array(
      '#type' => 'select',
      '#title' => t('View'),
      '#options' => $views,
      '#default_value' => $this->options['subviews_view'],
      '#description' => t('Select the view as the subview to use for this display.'),
    );

    // Arguments.
    $form['subviews_arguments'] = array(
      '#markup' => '<h2>' . t('Arguments') . '</h2>',
    );

    // Get a list of all available fields to use for arguments.
    $fields_info = $this->subviews_get_fields();
    foreach ($fields_info as $field => $field_name) {
      $fields[$field] = $field_name;
    }
    asort($fields);

    $fields = array('none' => t('None')) + $fields;

    // Create dropdowns to select fields to be used for 5 possible arguments.
    for ($i = 1; $i <= 5; $i++) {
      $form['subviews_argument_' . $i] = array(
        '#type' => 'select',
        '#title' => t('Argument @number', array('@number' => $i)),
        '#options' => $fields,
        '#default_value' => $this->options['subviews_argument_' . $i],
        '#description' => t('Select the field to use for this argument.'),
      );
    }

    // General setting
    $form['subviews_settings_header'] = array(
      '#markup' => '<h2>' . t('General settings') . '</h2>',
    );

    // Subviews behavior option.
    $form['subviews_behavior'] = array(
      '#type' => 'select',
      '#title' => t('Subviews behavior'),
      '#options' => array(
        'ajax' => t('AJAX enabled expand/close'),
        'noajax' => t('Expand/close functionality without AJAX loading'),
        'noexpand' => t('Always expanded'),
      ),
      '#default_value' => $this->options['subviews_behavior'],
      '#description' => t('Set the behavior of the subviews.'),
    );

    // Remember state option.
    $form['subviews_remember'] = array(
      '#type' => 'select',
      '#title' => t('Remember option'),
      '#options' => array(
        'noremember' => t("Don't save open/closed state"),
        'remember_session' => t('Save the open/closed state in the current session'),
      ),
      '#default_value' => $this->options['subviews_remember'],
      '#description' => t('Choose if this subview should remember the display state of a subview.'),
    );

    $form['subviews_wrapper_close'] = array(
      '#markup' => '</div>',
    );
  }

  // Run any validation on the form settings.
  function options_validate(&$form, &$form_state) {
    module_load_all_includes('subviews.inc');

    $arguments = array(
      &$form,
      &$form_state,
      &$this,
    );

    // Call all modules that use hook_subviews_options_form_validate
    foreach (module_implements('subviews_options_form_validate') as $module) {
      $function = $module . '_subviews_options_form_validate';
      call_user_func_array($function, $arguments);
    }
  }

  // Run any necessary actions on submit.
  function options_submit(&$form, &$form_state) {
    module_load_all_includes('subviews.inc');

    $arguments = array(
      $form,
      &$form_state,
    );

    // Call all modules that use hook_subviews_options_form_submit
    foreach (module_implements('subviews_options_form_submit') as $module) {
      $function = $module . '_subviews_options_form_submit';
      call_user_func_array($function, $arguments);
    }

  }

  // Retrieve a list of all available views and displays in the system.
  function subviews_get_views() {
    static $views;

    if (empty($views)) {
      $views = array();

      $views_list = views_get_all_views();

      foreach ($views_list as $name => $view) {
        $displays = $view->display_objects();

        foreach ($view->display as $disp_name => $display) {
          $views[$name . '-' . $disp_name] = $view->get_human_name() . ' - ' . $display->display_title;
        }
      }
    }

    return $views;
  }
  
  // Retrieve a list of all available fields in the view.
  function subviews_get_fields() {
    static $fields;

    if (empty($fields)) {
      $fields = array();
      $field_names = $this->display->handler->get_field_labels();

      foreach ($field_names as $name => $field) {
        $fields[$name] = $field;
      }
    }

    return $fields;
  }
}
