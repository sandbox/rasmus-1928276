<?php

/**
 * @file
 * Template to display a view as a subviews table.
 *
 * - $title : The title of this group of rows. May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $noexpand: A true/false value - TRUE = subview is not expandable,
 *   FALSE = subview is expandable.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $row_subview_classes: An array of classes to apply to each subview row,
 *   indexed by row.
 *   number. This matches the index in $rows.
 * - $row_subview_id: An array of ids to apply to each subview row,
 *   indexed by row.
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<table <?php if ($classes) : print 'class="' . $classes . '" ';endif; ?><?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php if (!$noexpand) : ?>
          <th class="subviews-toggle">+/-</th>
        <?php endif; ?>
        <?php foreach ($header as $field => $label): ?>
          <th <?php if ($header_classes[$field]) : print 'class="' . $header_classes[$field] . '" ';endif;?>>
            <?php print $label; ?>
          </th>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) : print 'class="' . implode(' ', $row_classes[$row_count]) . '"';endif; ?>>
        <?php if (!$noexpand) : ?>
          <td class="subviews-toggle"><?php print $subviews_link[$row_count]; ?></td>
        <?php endif; ?>
        <?php foreach ($row as $field => $content): ?>
          <td <?php if ($field_classes[$field][$row_count]) : print 'class="' . $field_classes[$field][$row_count] . '" ';endif; ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
      <tr <?php if ($row_subview_classes[$row_count]) : print 'class="' . implode(' ', $row_subview_classes[$row_count]) . '"';endif; ?> <?php if ($row_subview_id[$row_count]) : print 'id="' . $row_subview_id[$row_count] . '"';endif; ?> <?php if (!$row_expanded[$row_count]) : print 'style="display:none;"';endif; ?>>
        <?php if (!$noexpand) : ?>
          <td></td>
        <?php endif; ?>
        <td colspan="<?php print $column_count; ?>"><?php print $subviews[$row_count]; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
