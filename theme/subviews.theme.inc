<?php
/**
 * @file
 * Theme functions
 */

/**
 * Preprocess theme implementation for the subviews based on the table preprocess function. 
 */
function template_preprocess_subviews(&$vars) {
  $view     = $vars['view'];

  $module_path = drupal_get_path('module', 'subviews');

  // We need the raw data for this grouping, which is passed in as $vars['rows'].
  // However, the template also needs to use for the rendered fields.  We
  // therefore swap the raw data out to a new variable and reset $vars['rows']
  // so that it can get rebuilt.
  // Store rows so that they may be used by further preprocess functions.
  $result                     = $vars['result'] = $vars['rows'];
  $vars['rows']               = array();
  $vars['field_classes']      = array();
  $vars['field_attributes']   = array();
  $vars['header']             = array();
  $vars['column_count']       = 0;
  $vars['subviews']           = array();
  $vars['row_expanded']       = array();

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;

  $subview  = isset($options['subviews_view']) ? $options['subviews_view'] : NULL;

  if (!$subview) {
    drupal_set_message(t('No subview set for this view.'), 'error');
  }
  else {
    $default_row_class   = isset($options['default_row_class']) ? $options['default_row_class'] : TRUE;
    $row_class_special   = isset($options['row_class_special']) ? $options['row_class_special'] : TRUE;
    $behavior            = isset($options['subviews_behavior']) ? $options['subviews_behavior'] : 'ajax';
    $remember            = isset($options['subviews_remember']) ? $options['subviews_remember'] : 'noremember';

    // We need these to ensure subviews will work without JavaScript.
    $reqpath = isset($_GET['reqpath']) ? $_GET['reqpath'] : request_path();
    $expand  = isset($_GET['expand']) ? $_GET['expand'] : NULL;

    // Make sure at least one argument is passed to the subview
    // if expand/collapse behavior is selected.
    if ($behavior != 'noexpand') {
      $arg_field_set = FALSE;

      for ($i = 1; $i <= 5; $i++) {
        $arg_field_val = $options['subviews_argument_' . $i];
        $arg_field_set = $arg_field_val != 'none' ? TRUE : $arg_field_set;
      }

      if (!$arg_field_set) {
        drupal_set_message(t('To use expand/collapse functionality, at least one argument must be passed to the subview!'), 'warning');
        $behavior = 'noexpand';
      }
    }

    if ($view->editing == 1) {
      $behavior = 'noexpand';
      $vars['title'] = t('AJAX functionality will not work in preview mode!');
    }

    $vars['noexpand']    = $behavior == 'noexpand' ? TRUE : FALSE;

    // Load JS for expand/collapse functionality if nescessary.
    if (!$vars['noexpand']) {
      drupal_add_js($module_path . '/js/subviews.js');
    }

    $subviews_opt = explode('-', $subview);

    $fields   = &$view->field;
    $columns  = $handler->sanitize_columns($options['columns'], $fields);

    $active   = !empty($handler->active) ? $handler->active : '';
    $order    = !empty($handler->order) ? $handler->order : 'asc';

    $query    = tablesort_get_query_parameters();

    if (isset($view->exposed_raw_input)) {
      $query += $view->exposed_raw_input;
    }

    // Fields must be rendered in order as of Views 2.3, so we will
    // pre-render everything.
    $renders = $handler->render_fields($result);

    foreach ($columns as $field => $column) {
      // Create a second variable so we can easily find what fields we have
      // and what theCSS classes should be.
      $vars['fields'][$field] = drupal_clean_css_identifier($field);
      if ($active == $field) {
        $vars['fields'][$field] .= ' active';
      }

      // Render the header labels.
      if ($field == $column && empty($fields[$field]->options['exclude'])) {
        $vars['column_count']++;

        $label = check_plain(!empty($fields[$field]) ? $fields[$field]->label() : '');
        if (empty($options['info'][$field]['sortable']) || !$fields[$field]->click_sortable()) {
          $vars['header'][$field] = $label;
        }
        else {
          $initial = !empty($options['info'][$field]['default_sort_order']) ? $options['info'][$field]['default_sort_order'] : 'asc';

          if ($active == $field) {
            $initial = ($order == 'asc') ? 'desc' : 'asc';
          }

          $title = t('sort by @s', array('@s' => $label));
          if ($active == $field) {
            $label .= theme('tablesort_indicator', array('style' => $initial));
          }

          $query['order'] = $field;
          $query['sort'] = $initial;
          $link_options = array(
            'html' => TRUE,
            'attributes' => array('title' => $title),
            'query' => $query,
          );
          $vars['header'][$field] = l($label, $_GET['q'], $link_options);
        }

        $vars['header_classes'][$field] = '';
        // Set up the header label class.
        if ($fields[$field]->options['element_default_classes']) {
          $vars['header_classes'][$field] .= "views-field views-field-" . $vars['fields'][$field];
        }
        $class = $fields[$field]->element_label_classes(0);
        if ($class) {
          if ($vars['header_classes'][$field]) {
            $vars['header_classes'][$field] .= ' ';
          }
          $vars['header_classes'][$field] .= $class;
        }
        // Add a CSS align class to each field if one was set.
        if (!empty($options['info'][$field]['align'])) {
          $vars['header_classes'][$field] .= ' ' . drupal_clean_css_identifier($options['info'][$field]['align']);
        }

        // Add a header label wrapper if one was selected.
        if ($vars['header'][$field]) {
          $element_label_type = $fields[$field]->element_label_type(TRUE, TRUE);
          if ($element_label_type) {
            $vars['header'][$field] = '<' . $element_label_type . '>' . $vars['header'][$field] . '</' . $element_label_type . '>';
          }
        }

      }

      // Add a CSS align class to each field if one was set.
      if (!empty($options['info'][$field]['align'])) {
        $vars['fields'][$field] .= ' ' . drupal_clean_css_identifier($options['info'][$field]['align']);
      }

      // Render each field into its appropriate column.
      foreach ($result as $num => $row) {
        $subviews_opt_row = $subviews_opt;

        // Add field classes.
        $vars['field_classes'][$field][$num] = '';
        if ($fields[$field]->options['element_default_classes']) {
          $vars['field_classes'][$field][$num] = "views-field views-field-" . $vars['fields'][$field];
        }
        if ($classes = $fields[$field]->element_classes($num)) {
          if ($vars['field_classes'][$field][$num]) {
            $vars['field_classes'][$field][$num] .= ' ';
          }

          $vars['field_classes'][$field][$num] .= $classes;
        }
        $vars['field_attributes'][$field][$num] = array();

        if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
          $field_output = $renders[$num][$field];
          $element_type = $fields[$field]->element_type(TRUE, TRUE);
          if ($element_type) {
            $field_output = '<' . $element_type . '>' . $field_output . '</' . $element_type . '>';
          }

          // Don't bother with separators and stuff if the field doesn't show up.
          if (empty($field_output) && !empty($vars['rows'][$num][$column])) {
            continue;
          }

          // Place the field into the column, along with an optional separator.
          if (!empty($vars['rows'][$num][$column])) {
            if (!empty($options['info'][$column]['separator'])) {
              $vars['rows'][$num][$column] .= filter_xss_admin($options['info'][$column]['separator']);
            }
          }
          else {
            $vars['rows'][$num][$column] = '';
          }

          $vars['rows'][$num][$column] .= $field_output;
        }

        for ($i = 1; $i <= 5; $i++) {

          $arg_field_val = $options['subviews_argument_' . $i];
          $arg_field = $arg_field_val != 'none' ? $arg_field_val : NULL;

          if ($arg_field) {
            $subviews_opt_row[] = strip_tags($renders[$num][$arg_field]);
          }
          else {
            $subviews_opt_row[] = NULL;
          }
        }

        $hash = _subviews_hash($subviews_opt_row);

        if ($behavior == 'noexpand' || ($remember == 'remember_session' && _subviews_expanded($hash))) {
          $vars['row_expanded'][$num] = TRUE;
        }
        else {
          $vars['row_expanded'][$num] = FALSE;
        }

        // Decide if a toggle button should be displayed and what the state should be.
        if ($behavior == 'noexpand') {
          $subview_link = '';
        }
        elseif ($remember == 'remember_session' && $vars['row_expanded'][$num]) {
          $subview_link = _subviews_toggle_button('close', $hash, $subviews_opt_row, $reqpath);
        }
        elseif ($behavior == 'noajax') {
          $subview_link = _subviews_toggle_button('open', $hash, $subviews_opt_row, $reqpath);
        }
        else {
          $subview_link = _subviews_toggle_button('load', $hash, $subviews_opt_row, $reqpath);
        }

        $vars['subviews_link'][$num] = $subview_link;

        if ($behavior == 'ajax' && !$vars['row_expanded'][$num] && $hash != $expand) {
          $vars['subviews'][$num] = '<div id="subviews-load-' . $hash . '"></div>';
        }
        else {
          $output = call_user_func_array('views_embed_view', $subviews_opt_row);
          $vars['subviews'][$num] = '<div id="subviews-load-' . $hash . '">' . $output . '</div>';
        }
        $vars['row_subview_id'][$num] = 'views-row-' . $hash;
      }

      // Remove columns if the option is hide empty column is checked
      // and the field is not empty.
      if (!empty($options['info'][$field]['empty_column'])) {
        $empty = TRUE;
        foreach ($vars['rows'] as $num => $columns) {
          $empty &= empty($columns[$column]);
        }
        if ($empty) {
          foreach ($vars['rows'] as $num => &$column_items) {
            unset($column_items[$column]);
            unset($vars['header'][$column]);
          }
        }
      }
    }

    // Hide table header if all labels are empty.
    if (!array_filter($vars['header'])) {
      $vars['header'] = array();
    }

    $count = 0;
    foreach ($vars['rows'] as $num => $row) {
      $vars['row_classes'][$num] = array();
      $vars['row_subview_classes'][$num] = array();
      if ($row_class_special) {
        $row_highlight_class = ($count++ % 2 == 0) ? 'odd' : 'even';

        $vars['row_classes'][$num][] = $row_highlight_class;
        $vars['row_subview_classes'][$num][] = $row_highlight_class;
      }
      if ($row_class = $handler->get_row_class($num)) {
        $vars['row_classes'][$num][] = $row_class;
        $vars['row_subview_classes'][$num][] = $row_class;
      }
      $vars['row_classes'][$num][] = 'views-row';
      $vars['row_subview_classes'][$num][] = 'views-row-subview';

      if (!$vars['row_expanded'][$num]) {
        $vars['row_subview_classes'][$num][] = 'views-row-hide';
      }
    }

    if ($row_class_special) {
      $vars['row_classes'][0][] = 'views-row-first';
      $vars['row_classes'][count($vars['row_classes']) - 1][] = 'views-row-last';
      $vars['row_subview_classes'][0][] = 'views-row-first';
      $vars['row_subview_classes'][count($vars['row_subview_classes']) - 1][] = 'views-row-last';
    }

    $vars['classes_array'] = array('views-table');
    if (empty($vars['rows']) && !empty($options['empty_table'])) {
      $vars['rows'][0][0] = $view->display_handler->render_area('empty');
      // Calculate the amounts of rows with output.
      $vars['field_attributes'][0][0]['colspan'] = count($vars['header']);
      $vars['field_classes'][0][0] = 'views-empty';
    }

    if (!empty($options['sticky'])) {
      drupal_add_js('misc/tableheader.js');
      $vars['classes_array'][] = "sticky-enabled";
    }
    $vars['classes_array'][] = 'cols-' . count($vars['header']);

    if (!empty($handler->options['summary'])) {
      $vars['attributes_array'] = array('summary' => $handler->options['summary']);
    }
  }
}


/**
 * The toggle button.
 */
function theme_subviews_toggle_button($vars) {
  if ($vars['state'] == 'open') {
    return '+';
  }
  else {
    return '-';
  }
}
